import React from "react"
import BattleScreen from "./gameScreens/battleScreen";
import CharacterSelectionScreen from "./gameScreens/characterSelectionScreen";
import LoginScreen from "./gameScreens/loginScreen";
import TownScreen from "./gameScreens/townScreen";
import TrophiesScreen from "./gameScreens/trophiesScreen";

class Game extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      achievements: [],
      armorUpgrades: [],
      battlingMonster: null,
      currentUser: null,
      currentCharacterSheet: null,
      isCharacterSelectionScreenActive: false,
      isBattleScreenActive: false,
      isLoginScreenActive: true,
      isTownScreenActive: false,
      isTrophiesScreenActive: false,
      monsters: [],
      weaponUpgrades: [],
    }
  }

  componentDidMount() {
    this.fetchMonsters();
    this.fetchArmorUpgrades();
    this.fetchWeaponUpgrades();
    this.fetchAchievements();
  }

  render() {
    return (
      <div>
        {this.state.isLoginScreenActive && this.renderLoginScreen()}
        {this.state.isCharacterSelectionScreenActive && this.renderCharacterSelectionScreen()}
        {this.state.isTownScreenActive && this.renderTownScreen()}
        {this.state.isBattleScreenActive && this.renderBattleScreen()}
        {this.state.isTrophiesScreenActive && this.renderTrophiesScreen()}
      </div>
    );
  }

  renderLoginScreen() {
    return (
      <LoginScreen onLoginSuccess={this.setCurrentUser.bind(this)}/>
    );
  }

  setCurrentUser(currentUser) {
    this.setState({
      currentUser: currentUser,
      currentCharacterSheet: null,
      isCharacterSelectionScreenActive: true,
      isLoginScreenActive: false
    });
  }

  renderCharacterSelectionScreen() {
    return (
      <CharacterSelectionScreen
        currentUser={this.state.currentUser}
        onCharacterSelected={this.setCurrentCharacterSheet.bind(this)}/>
    );
  }

  setCurrentCharacterSheet(currentCharacterSheet) {
    return this.setState({
      currentCharacterSheet: currentCharacterSheet,
      isCharacterSelectionScreenActive: false,
      isTownScreenActive: true
    });
  }

  renderTownScreen() {
    return (
      <TownScreen
        achievements={this.state.achievements}
        armorUpgrades={this.state.armorUpgrades}
        characterSheet={this.state.currentCharacterSheet}
        monsters={this.state.monsters}
        nextArmorUpgrade={this.getNextArmorUpgrade()}
        nextWeaponUpgrade={this.getNextWeaponUpgrade()}
        onCharacterUpgrade={this.updateCharacterSheet.bind(this)}
        onMonsterSelected={this.startBattle.bind(this)}
        onNotificationClosed={this.fetchUser.bind(this)}
        onTrophiesListRequested={this.showTrophiesList.bind(this)}
        user={this.state.currentUser}
        weaponUpgrades={this.state.weaponUpgrades}/>
    );
  }

  renderBattleScreen() {
    return (
      <BattleScreen
        characterSheet={this.state.currentCharacterSheet}
        monster={this.state.battlingMonster}
        onPlayerDefeat={this.punishPlayerDefeat.bind(this)}
        onPlayerVictory={this.rewardPlayerVictory.bind(this)}/>
    );
  }

  renderTrophiesScreen() {
    return (
      <TrophiesScreen
        achievements={this.state.achievements}
        onWindowClose={this.closeTrophiesScreen.bind(this)}
        trophies={this.state.currentUser.trophies}/>
    );
  }

  startBattle(monster) {
    return this.setState({
      battlingMonster: monster,
      isBattleScreenActive: true
    });
  }

  closeTrophiesScreen() {
    return this.setState({
      isTrophiesScreenActive: false
    });
  }

  showTrophiesList() {
    return this.setState({
      isTrophiesScreenActive: true
    });
  }

  updateCharacterSheet(characterSheet) {
    return this.setState({
      currentCharacterSheet: characterSheet
    });
  }

  punishPlayerDefeat() {
    const requestUrl = `/api/character_sheets/${this.state.currentCharacterSheet.id}/deaths`;
    const requestMethod = 'POST';
    const requestBody = JSON.stringify({monster_id: this.state.battlingMonster.id});
    const requestHeaders = {Accept: 'application/json', 'Content-Type': 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod, body: requestBody};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then((updatedCharacterSheet) => {
        this.setState({
          isBattleScreenActive: false,
          battlingMonster: null,
          currentCharacterSheet: updatedCharacterSheet
        }, this.fetchUser);
      });
    });
  }

  rewardPlayerVictory() {
    const requestUrl = `/api/character_sheets/${this.state.currentCharacterSheet.id}/killed_monsters`;
    const requestMethod = 'POST';
    const requestBody = JSON.stringify({monster_id: this.state.battlingMonster.id});
    const requestHeaders = {Accept: 'application/json', 'Content-Type': 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod, body: requestBody};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then((updatedCharacterSheet) => {
        this.setState({
          isBattleScreenActive: false,
          battlingMonster: null,
          currentCharacterSheet: updatedCharacterSheet
        }, this.fetchUser);
      });
    });
  }

  fetchMonsters() {
    const requestUrl = `/api/monsters`;
    const requestMethod = 'GET';
    const requestHeaders = {Accept: 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then((monsters) => this.setState({monsters: monsters}));
    });
  }

  fetchArmorUpgrades() {
    const requestUrl = `/api/armor_upgrades`;
    const requestMethod = 'GET';
    const requestHeaders = {Accept: 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then((armorUpgrades) => this.setState({armorUpgrades: armorUpgrades}));
    });
  }

  fetchWeaponUpgrades() {
    const requestUrl = `/api/weapon_upgrades`;
    const requestMethod = 'GET';
    const requestHeaders = {Accept: 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then((weaponUpgrades) => this.setState({weaponUpgrades: weaponUpgrades}));
    });
  }

  fetchAchievements() {
    const requestUrl = `/api/achievements`;
    const requestMethod = 'GET';
    const requestHeaders = {Accept: 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then((achievements) => this.setState({achievements: achievements}));
    });
  }

  fetchUser() {
    const requestUrl = `/api/users/${this.state.currentUser.username}`;
    const requestMethod = 'GET';
    const requestHeaders = {Accept: 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then((user) => this.setState({currentUser: user}));
    });
  }

  getNextArmorUpgrade() {
    const currentArmorUpgrade = this.state.currentCharacterSheet.armor_upgrade;
    const nextLevel = !!currentArmorUpgrade ? currentArmorUpgrade.level + 1 : 1;

    return this.state.armorUpgrades.find((armor) => {
      return armor.level === nextLevel;
    });
  }

  getNextWeaponUpgrade() {
    const currentWeaponUpgrade = this.state.currentCharacterSheet.weapon_upgrade;
    const nextLevel = !!currentWeaponUpgrade ? currentWeaponUpgrade.level + 1 : 1;

    return this.state.weaponUpgrades.find((weapon) => {
      return weapon.level === nextLevel;
    });
  }
}

export default Game
