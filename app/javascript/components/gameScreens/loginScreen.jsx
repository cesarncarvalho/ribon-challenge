import React from "react"
import Card from "../sharedComponents/card";
import PropTypes from "prop-types";

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: null
    }
  }

  render() {
    return (
      <div className="screen login-screen">
        <Card className="login-form">
          <form action="/" onSubmit={this.fetchExistingUser.bind(this)}>
            <label className="label" htmlFor="username">Digite seu nome de usuário para começar a jogar</label>
            <input autoFocus={true} className="input" id="username" name="username" onChange={this.setUserEmail.bind(this)} type="text"/>
            <button className="button" onClick={this.fetchExistingUser.bind(this)} type="submit">Jogar</button>
          </form>
        </Card>
      </div>
    );
  }

  fetchExistingUser(event) {
    event.preventDefault();

    const requestUrl = `/api/users/${this.state.username}`;
    const requestMethod = 'GET';
    const requestHeaders = {Accept: 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod};

    fetch(requestUrl, requestOptions).then((response) => {
      const userNotFound = response.status === 404;

      if (userNotFound) {
        return this.createUser();
      }

      response.json().then(this.props.onLoginSuccess);
    });
  }

  createUser() {
    const requestUrl = '/api/users';
    const requestMethod = 'POST';
    const requestBody = JSON.stringify({username: this.state.username});
    const requestHeaders = {Accept: 'application/json', 'Content-Type': 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod, body: requestBody};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then(this.props.onLoginSuccess);
    });
  }

  setUserEmail(event) {
    this.setState({username: event.currentTarget.value});
  }
}

LoginScreen.propTypes = {
  onLoginSuccess: PropTypes.func.isRequired
};

export default LoginScreen
