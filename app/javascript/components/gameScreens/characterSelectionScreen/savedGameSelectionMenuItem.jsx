import React from "react"
import Card from "../../sharedComponents/card";
import PropTypes from "prop-types";

class SavedGameSelectionMenuItem extends React.Component {
  render() {
    return (
      <Card className="saved-game-selection-menu-item">
        <span className="class-name">{this.props.characterSheet.character_class.name}</span>
        <div className="attributes-list">
          <p className="attribute">
            <span className="attribute-name">Criado em:</span>
            <span>{new Date(this.props.characterSheet.created_at).toLocaleString()}</span>
          </p>
          <p className="attribute">
            <span className="attribute-name">Inimigos derrotados:</span>
            <span>{this.props.characterSheet.defeated_monsters}</span>
          </p>
          <p className="attribute">
            <span className="attribute-name">Moedas:</span>
            <span>{this.props.characterSheet.coins}</span>
          </p>
          <p className="attribute">
            <span className="attribute-name">Inimigos derrotados:</span>
            <span>{this.props.characterSheet.defeated_monsters}</span>
          </p>
          <p className="attribute">
            <span className="attribute-name">Derrotas:</span>
            <span>{this.props.characterSheet.deaths}</span>
          </p>
        </div>
        <button className="button" onClick={this.selectSavedGame.bind(this)} type="button">Continuar</button>
      </Card>
    );
  }

  selectSavedGame() {
    return this.props.onGameSelected(this.props.characterSheet);
  }
}

SavedGameSelectionMenuItem.propTypes = {
  characterSheet: PropTypes.object.isRequired,
  onGameSelected: PropTypes.func.isRequired
};

export default SavedGameSelectionMenuItem
