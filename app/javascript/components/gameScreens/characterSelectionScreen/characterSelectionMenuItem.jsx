import React from "react"
import Card from "../../sharedComponents/card";
import PropTypes from "prop-types";

class CharacterSelectionMenuItem extends React.Component {
  render() {
    return (
      <Card className="character-selection-menu-item">
        <div className="character-image">
          <img src={this.props.characterClass.animation_url} alt={this.props.characterClass.name}/>
        </div>
        <span className="class-name">{this.props.characterClass.name}</span>
        <span className="description">{this.props.characterClass.description}</span>
        <div className="attributes-list">
          <p className="attribute">
            <span className="attribute-name">Bônus de ataque:</span>
            <span>{this.props.characterClass.attack_bonus}</span>
          </p>
          <p className="attribute">
            <span className="attribute-name">Dano:</span>
            <span>{this.props.characterClass.damage}</span>
          </p>
          <p className="attribute">
            <span className="attribute-name">Pontos de vida:</span>
            <span>{this.props.characterClass.hit_points}</span>
          </p>
        </div>
        <button className="button" onClick={this.selectCharacter.bind(this)} type="button">Selecionar</button>
      </Card>
    );
  }

  selectCharacter() {
    return this.props.onCharacterSelected(this.props.characterClass.id);
  }
}

CharacterSelectionMenuItem.propTypes = {
  characterClass: PropTypes.object.isRequired,
  onCharacterSelected: PropTypes.func.isRequired
};

export default CharacterSelectionMenuItem
