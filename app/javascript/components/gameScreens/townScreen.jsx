import React from "react"
import MonsterSelectionMenu from "./townScreen/monsterSelectionMenu";
import PropTypes from "prop-types";
import TrophiesNotificationsList from "./townScreen/trophiesNotificationList";

class TownScreen extends React.Component {
  render() {
    return (
      <div className="screen town-screen">
        <div className="game-menu">
          {this.renderCharacterStatus()}
          {this.props.nextWeaponUpgrade && this.renderWeaponUpgrade()}
          {this.props.nextArmorUpgrade && this.renderArmorUpgrade()}
          {this.renderPlayerTrophies()}
        </div>
        {this.renderMonsterSelectionMenu()}
        {this.renderNotificationsList()}
      </div>
    );
  }

  renderMonsterSelectionMenu() {
    return (
      <MonsterSelectionMenu
        monsters={this.props.monsters}
        onMonsterSelected={this.props.onMonsterSelected}/>
    );
  }

  upgradeArmor(event) {
    event.preventDefault();

    const requestUrl = `/api/character_sheets/${this.props.characterSheet.id}/armor_upgrade`;
    const requestMethod = 'PUT';
    const requestBody = JSON.stringify({armor_upgrade_id: this.props.nextArmorUpgrade.id});
    const requestHeaders = {Accept: 'application/json', 'Content-Type': 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod, body: requestBody};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then(this.props.onCharacterUpgrade);
    });
  }

  upgradeWeapon(event) {
    event.preventDefault();

    const requestUrl = `/api/character_sheets/${this.props.characterSheet.id}/weapon_upgrade`;
    const requestMethod = 'PUT';
    const requestBody = JSON.stringify({weapon_upgrade_id: this.props.nextWeaponUpgrade.id});
    const requestHeaders = {Accept: 'application/json', 'Content-Type': 'application/json'};
    const requestOptions = {headers: requestHeaders, method: requestMethod, body: requestBody};

    fetch(requestUrl, requestOptions).then((response) => {
      response.json().then(this.props.onCharacterUpgrade);
    });
  }

  renderCharacterStatus() {
    return (
      <div className="attributes-list">
        <div className="title">Status do personagem</div>
        <div className="attribute">
          <span className="attribute-name">Moedas:</span>
          <span className="attribute-value">{this.props.characterSheet.coins}</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Inimigos derrotados:</span>
          <span className="attribute-value">{this.props.characterSheet.defeated_monsters}</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Derrotas:</span>
          <span className="attribute-value">{this.props.characterSheet.deaths}</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Bônus de ataque:</span>
          <span className="attribute-value">{this.props.characterSheet.total_attack_bonus}</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Dano do ataque:</span>
          <span className="attribute-value">{this.props.characterSheet.total_damage}</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Pontos de vida:</span>
          <span className="attribute-value">{this.props.characterSheet.total_hit_points}</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Melhoria de arma:</span>
          <span
            className="attribute-value">{!!this.props.characterSheet.weapon_upgrade ? `Nível ${this.props.characterSheet.weapon_upgrade.level}` : 'Nenhum'}</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Melhoria de armadura:</span>
          <span
            className="attribute-value">{!!this.props.characterSheet.armor_upgrade ? `Nível ${this.props.characterSheet.armor_upgrade.level}` : 'Nenhum'}</span>
        </div>
      </div>
    );
  }

  renderArmorUpgrade() {
    const upgradePrice = this.props.nextArmorUpgrade.price;
    const playerCanAfford = upgradePrice <= this.props.characterSheet.coins;

    return (
      <div className="attributes-list">
        <div className="title">Próxima melhoria de armadura</div>
        <div className="attribute">
          <span className="attribute-name">Nível:</span>
          <span className="attribute-value">{this.props.nextArmorUpgrade.level}</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Preço:</span>
          <span className="attribute-value">{upgradePrice} moedas</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Bônus de saúde:</span>
          <span className="attribute-value">pontos de vida x{this.props.nextArmorUpgrade.health_bonus}</span>
        </div>
        {playerCanAfford && <a onClick={this.upgradeArmor.bind(this)} href="#">Comprar melhoria</a>}
      </div>
    );
  }

  renderWeaponUpgrade() {
    const upgradePrice = this.props.nextWeaponUpgrade.price;
    const playerCanAfford = upgradePrice <= this.props.characterSheet.coins;

    return (
      <div className="attributes-list">
        <div className="title">Próxima melhoria de arma</div>
        <div className="attribute">
          <span className="attribute-name">Nível:</span>
          <span className="attribute-value">{this.props.nextWeaponUpgrade.level}</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Preço:</span>
          <span className="attribute-value">{this.props.nextWeaponUpgrade.price} moedas</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Bônus de ataque:</span>
          <span className="attribute-value">+{this.props.nextWeaponUpgrade.attack_bonus}</span>
        </div>
        <div className="attribute">
          <span className="attribute-name">Bônus de dano:</span>
          <span className="attribute-value">x{this.props.nextWeaponUpgrade.damage_bonus}</span>
        </div>
        {playerCanAfford && <a onClick={this.upgradeWeapon.bind(this)} href="#">Comprar melhoria</a>}
      </div>
    );
  }

  renderPlayerTrophies() {
    return (
      <div className="attributes-list">
        <div className="title">Troféus</div>
        <div className="attribute">
          <span className="attribute-value">
            Você completou {this.props.user.trophies.length} de {this.props.achievements.length} desafios
          </span>
        </div>
        <a onClick={this.props.onTrophiesListRequested.bind(this)} href="#">Exibir todos os desafios</a>
      </div>
    );
  }

  renderNotificationsList() {
    return (
      <TrophiesNotificationsList
        userTrophies={this.props.user.trophies}
        onNotificationClosed={this.props.onNotificationClosed}/>
    );
  }
}

TownScreen.propTypes = {
  achievements: PropTypes.array.isRequired,
  armorUpgrades: PropTypes.array.isRequired,
  characterSheet: PropTypes.object.isRequired,
  monsters: PropTypes.array.isRequired,
  nextArmorUpgrade: PropTypes.object,
  nextWeaponUpgrade: PropTypes.object,
  onCharacterUpgrade: PropTypes.func.isRequired,
  onMonsterSelected: PropTypes.func.isRequired,
  onNotificationClosed: PropTypes.func.isRequired,
  onTrophiesListRequested: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  weaponUpgrades: PropTypes.array.isRequired
};

export default TownScreen
