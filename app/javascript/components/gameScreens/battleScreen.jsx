import React from "react"
import PropTypes from "prop-types";

class BattleScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      playerCurrentHealth: props.characterSheet.total_hit_points,
      isBattleOutcomeVisible: false,
      monsterCurrentHealth: props.monster.monster_sheet.hit_points,
      playerRoll: null,
      totalPlayerAttack: null,
      playerDefeated: false,
      monsterRoll: null,
      monsterDefeated: false,
      totalMonsterAttack: null,
      turnResultMessage: null,
      disableContinueButton: false
    }
  }

  render() {
    return (
      <div className="battle-screen">
        <div className="battle-map" style={{backgroundImage: `url('${this.getDungeonImage()}')`}}>
          {this.renderOpponentsSheets()}
          <div className="animations">
            {this.renderPlayerAnimation()}
            {this.state.isBattleOutcomeVisible ? this.renderBattleOutcome() : <div className="battle-actions"/>}
            {this.renderMonsterAnimation()}
          </div>
        </div>
      </div>
    );
  }

  renderBattleOutcome() {
    return (
      <div className="battle-actions">
        <div className="wrapper">
          <div className="rolls">
            <div className="roll">
              <span>Sua rolagem</span>
              <span>+{this.props.characterSheet.total_attack_bonus} de bônus</span>
              <div className="dice">
                <span>{this.state.playerRoll}</span>
              </div>
              <span>Total: {this.state.totalPlayerAttack}</span>
            </div>
            <div className="roll">
              <span>Rolagem do inimigo</span>
              <span>+{this.props.monster.monster_sheet.attack_bonus} de bônus</span>
              <div className="dice">
                <span>{this.state.monsterRoll}</span>
              </div>
              <span>Total: {this.state.totalMonsterAttack}</span>
            </div>
          </div>
          <div
            className={`result ${this.state.playerDefeated ? 'defeat' : ''} ${this.state.monsterDefeated ? 'victory' : ''}`}>
            <span>{this.state.turnResultMessage}</span>
          </div>
        </div>
      </div>
    );
  }

  renderMonsterAnimation() {
    return (
      <div className="monster">
        <div className="animation">
          <img src={this.props.monster.monster_sheet.animation_url} alt=""/>
        </div>
      </div>
    );
  }

  renderPlayerAnimation() {
    return (
      <div className="player">
        {!this.state.playerDefeated && !this.state.monsterDefeated && this.renderAttackButton()}
        {this.state.playerDefeated && this.renderPlayerDefeatedButton()}
        {this.state.monsterDefeated && this.renderMonsterDefeatedButton()}
        <div className="animation">
          <img src={this.props.characterSheet.character_class.animation_url} alt=""/>
        </div>
      </div>
    );
  }

  renderAttackButton() {
    return (
      <button className="button" onClick={this.rollAttack.bind(this)} type="button">Atacar!</button>
    );
  }

  renderOpponentsSheets() {
    return (
      <div className="sheets">
        <div className="sheet">
          <div className="attributes-list-horizontal">
            <div className="attribute">
              <span className="attribute-name">Pontos de vida</span>
              <span
                className="attribute-value">{this.state.playerCurrentHealth}/{this.props.characterSheet.total_hit_points}</span>
            </div>
            <div className="attribute">
              <span className="attribute-name">Bônus de ataque</span>
              <span className="attribute-value">{this.props.characterSheet.total_attack_bonus}</span>
            </div>
            <div className="attribute">
              <span className="attribute-name">Dano</span>
              <span className="attribute-value">{this.props.characterSheet.total_damage}</span>
            </div>
          </div>
        </div>
        <div className="sheet">
          <div className="attributes-list-horizontal">
            <div className="attribute">
              <span className="attribute-name">Pontos de vida</span>
              <span
                className="attribute-value">{this.state.monsterCurrentHealth}/{this.props.monster.monster_sheet.hit_points}</span>
            </div>
            <div className="attribute">
              <span className="attribute-name">Bônus de ataque</span>
              <span className="attribute-value">{this.props.monster.monster_sheet.attack_bonus}</span>
            </div>
            <div className="attribute">
              <span className="attribute-name">Dano</span>
              <span className="attribute-value">{this.props.monster.monster_sheet.damage}</span>
            </div>
            <div className="attribute">
              <span className="attribute-name">Recompensa</span>
              <span
                className="attribute-value">{this.props.monster.monster_sheet.reward.toLocaleString()} {this.props.monster.monster_sheet.reward > 1 ? 'moedas' : 'moeda'}</span>
            </div>
          </div>
        </div>
      </div>
    );
  }

  getDungeonImage() {
    const monsterLevel = this.props.monster.monster_sheet.level;

    switch (monsterLevel) {
      case 1:
        return '/backgrounds/terreno-goblin.png';
      case 2:
        return '/backgrounds/terreno-orc.jpg';
      case 3:
        return '/backgrounds/terreno-esqueleto.jpg';
      case 4:
        return '/backgrounds/terreno-lich.jpg';
      case 5:
        return '/backgrounds/terreno-dragao.jpg';
      default:
        return '/backgrounds/terreno-goblin.png';
    }
  }

  rollAttack() {
    const playerRoll = (Math.floor(Math.random() * 10 ** 6) % 20) + 1;
    const monsterRoll = (Math.floor(Math.random() * 10 ** 6) % 20) + 1;
    const totalPlayerAttack = playerRoll + this.props.characterSheet.total_attack_bonus;
    const totalMonsterAttack = monsterRoll + this.props.monster.monster_sheet.attack_bonus;

    let playerCurrentHealth = this.state.playerCurrentHealth;
    let monsterCurrentHealth = this.state.monsterCurrentHealth;
    let turnResultMessage = null;
    let playerDefeated = false;
    let monsterDefeated = false;

    if (totalPlayerAttack > totalMonsterAttack) {
      const playerTotalDamage = this.props.characterSheet.total_damage;
      monsterCurrentHealth -= Math.min(playerTotalDamage, monsterCurrentHealth);

      if (monsterCurrentHealth > 0) {
        turnResultMessage = `Você acertou e causou ${playerTotalDamage} de dano!`
      } else {
        const monsterReward = this.props.monster.monster_sheet.reward;

        turnResultMessage = `Você derrotou o monstro e ganhou ${monsterReward} ${monsterReward > 1 ? 'moedas' : 'moeda'}!`;
        monsterDefeated = true;
      }
    } else if (totalPlayerAttack < totalMonsterAttack) {
      const monsterTotalDamage = this.props.monster.monster_sheet.damage;
      playerCurrentHealth -= Math.min(monsterTotalDamage, playerCurrentHealth);

      if (playerCurrentHealth > 0) {
        turnResultMessage = `Você tomou ${monsterTotalDamage} de dano!`
      } else {
        let lostCoins = Math.floor(this.props.monster.monster_sheet.reward / 2);
        lostCoins = Math.min(lostCoins, this.props.characterSheet.coins);

        if (lostCoins === 0) {
          turnResultMessage = `Você foi derrotado porém não tinha mais moedas para perder.`;
        } else {
          turnResultMessage = `Você foi derrotado e perdeu ${lostCoins.toLocaleString()} ${lostCoins > 1 ? 'moedas' : 'moeda'}!`;
        }

        playerDefeated = true;
      }
    } else {
      turnResultMessage = 'Empate! Niguém sofre dano nessa rodada.'
    }

    this.setState({
      playerCurrentHealth: playerCurrentHealth,
      monsterCurrentHealth: monsterCurrentHealth,
      isBattleOutcomeVisible: true,
      playerRoll: playerRoll,
      totalPlayerAttack: totalPlayerAttack,
      playerDefeated: playerDefeated,
      monsterRoll: monsterRoll,
      totalMonsterAttack: totalMonsterAttack,
      monsterDefeated: monsterDefeated,
      turnResultMessage: turnResultMessage
    });
  }

  renderPlayerDefeatedButton() {
    return (
      <button className={`button ${this.state.disableContinueButton ? 'disabled' : ''}`}
              disabled={this.state.disableContinueButton} onClick={this.computeDefeat.bind(this)}
              type="button">Continuar</button>
    );
  }

  renderMonsterDefeatedButton() {
    return (
      <button className={`button ${this.state.disableContinueButton ? 'disabled' : ''}`}
              disabled={this.state.disableContinueButton} onClick={this.computeVictory.bind(this)}
              type="button">Continuar</button>
    );
  }

  computeDefeat() {
    this.setState({disableContinueButton: true}, this.props.onPlayerDefeat);
  }

  computeVictory() {
    this.setState({disableContinueButton: true}, this.props.onPlayerVictory);
  }
}

BattleScreen.propTypes = {
  characterSheet: PropTypes.object.isRequired,
  monster: PropTypes.object.isRequired,
  onPlayerDefeat: PropTypes.func.isRequired,
  onPlayerVictory: PropTypes.func.isRequired,
};

export default BattleScreen
