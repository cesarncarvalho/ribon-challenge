class Monster < ApplicationRecord
  has_one :monster_sheet

  has_many :killed_monsters

  validates_presence_of :name
end
