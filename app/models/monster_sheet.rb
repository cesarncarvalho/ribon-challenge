class MonsterSheet < ApplicationRecord
  belongs_to :monster

  validates_presence_of :animation_url, :attack_bonus, :damage, :hit_points, :level, :reward
end
