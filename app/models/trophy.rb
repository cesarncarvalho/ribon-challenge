class Trophy < ApplicationRecord
  belongs_to :achievement
  belongs_to :user

  validates_presence_of :conquered_at
end
