class WeaponUpgrade < ApplicationRecord
  has_many :character_sheets

  validates_presence_of :attack_bonus, :damage_bonus, :level, :price
end
