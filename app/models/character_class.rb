class CharacterClass < ApplicationRecord
  has_many :character_sheets

  validates_presence_of :animation_url, :attack_bonus, :damage, :description, :hit_points, :name
end
