class Api::CharacterSheets::KilledMonstersController < ActionController::API
  def create
    record = KillMonsterCommand.new(params[:character_sheet_id], params[:monster_id]).execute
    render json: record, serializer: CharacterSheetSerializer
  end
end
