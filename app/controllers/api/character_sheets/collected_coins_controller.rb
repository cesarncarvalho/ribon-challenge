class Api::CharacterSheets::CollectedCoinsController < ActionController::API
  def create
    record = CollectCoinsCommand.new(params[:character_sheet_id], params[:amount]).execute
    render json: record, serializer: CharacterSheetSerializer
  end
end
