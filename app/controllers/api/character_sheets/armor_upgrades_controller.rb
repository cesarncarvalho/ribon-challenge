class Api::CharacterSheets::ArmorUpgradesController < ActionController::API
  def update
    record = UpgradeCharacterArmorCommand.new(params[:character_sheet_id], params[:armor_upgrade_id]).execute
    render json: record, serializer: CharacterSheetSerializer
  end
end
