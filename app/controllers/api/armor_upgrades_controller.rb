class Api::ArmorUpgradesController < ActionController::API
  def index
    records = ListArmorUpgradesCommand.new.execute
    render json: records, each_serializer: ArmorUpgradeSerializer
  end
end
