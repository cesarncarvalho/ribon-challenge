class Api::UsersController < ActionController::API
  def create
    record = CreateUserCommand.new(params[:username]).execute
    render json: record, serializer: UserSerializer
  end

  def index
    record = ListUsersCommand.new.execute
    render json: record, each_serializer: UserSerializer
  end

  def show
    record = FindUserCommand.new(params[:username]).execute
    render json: record, serializer: UserSerializer
  end
end
