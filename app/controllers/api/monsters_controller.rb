class Api::MonstersController < ActionController::API
  def index
    records = ListMonstersCommand.new.execute
    render json: records, each_serializer: MonsterSerializer
  end

  def show
    record = FindMonsterCommand.new(params[:id]).execute
    render json: record, serializer: MonsterSerializer
  end
end
