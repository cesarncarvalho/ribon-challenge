class Api::AchievementsController < ActionController::API
  def index
    records = ListAchievementsCommand.new.execute
    render json: records, each_serializer: AchievementSerializer
  end
end
