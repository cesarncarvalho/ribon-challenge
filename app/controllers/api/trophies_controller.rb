class Api::TrophiesController < ActionController::API
  def update
    record = MarkTrophyAsNotifiedCommand.new(params[:id]).execute
    render json: record, serializer: TrophySerializer
  end

  def index
    records = ListUserTrophiesCommand.new(params[:user_id]).execute
    render json: records, each_serializer: TrophySerializer
  end
end
