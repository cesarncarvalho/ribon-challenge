class Api::CharacterClassesController < ActionController::API
  def index
    records = ListCharacterClassesCommand.new.execute
    render json: records, each_serializer: CharacterClassSerializer
  end
end
