class CharacterSheetSerializer < ActiveModel::Serializer
  attributes :armor_upgrade,
             :character_class,
             :coins,
             :created_at,
             :defeated_monsters,
             :deaths,
             :id,
             :total_attack_bonus,
             :total_damage,
             :total_hit_points,
             :updated_at,
             :weapon_upgrade

  belongs_to :armor_upgrade
  belongs_to :character_class
  belongs_to :weapon_upgrade
end
