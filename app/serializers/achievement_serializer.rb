class AchievementSerializer < ActiveModel::Serializer
  attributes :description,
             :id,
             :name,
             :target_quantity,
             :target_type
end
