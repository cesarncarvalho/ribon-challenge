class CharacterClassSerializer < ActiveModel::Serializer
  attributes :animation_url,
             :attack_bonus,
             :damage,
             :description,
             :hit_points,
             :id,
             :name
end
