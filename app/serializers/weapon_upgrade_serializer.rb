class WeaponUpgradeSerializer < ActiveModel::Serializer
  attributes :attack_bonus,
             :damage_bonus,
             :id,
             :level,
             :price
end
