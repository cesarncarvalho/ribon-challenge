class ListAchievementsCommand
  def execute
    Achievement.order(:target_type, :target_id, :target_quantity)
  end
end