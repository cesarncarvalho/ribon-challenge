class CollectTrophiesCommand
  def initialize(user_id, target_type, target_quantity, target_id = nil)
    @target_id = target_id
    @target_quantity = target_quantity
    @target_type = target_type
    @user_id = user_id
  end

  def execute
    matching_achievements = Achievement.where('target_quantity <= ? AND target_type = ?', @target_quantity, @target_type)

    if @target_id.present?
      matching_achievements = matching_achievements.where(target_id: @target_id)
    else
      matching_achievements = matching_achievements.where('target_id IS NULL')
    end

    matching_achievements.each do |achievement|
      trophy_already_conquered = Trophy.exists?(user_id: @user_id, achievement_id: achievement.id)

      if !trophy_already_conquered
        Trophy.create!(user_id: @user_id, achievement_id: achievement.id, conquered_at: DateTime.now)
      end
    end
  end
end