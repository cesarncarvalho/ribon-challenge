class FindMonsterCommand
  def initialize(monster_id)
    @monster_id = monster_id
  end

  def execute
    Monster.includes(:monster_sheet).find(@monster_id)
  end
end