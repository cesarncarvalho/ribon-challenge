class MarkTrophyAsNotifiedCommand
  def initialize(trophy_id)
    @trophy = Trophy.find(trophy_id)
  end

  def execute
    @trophy.update_attributes!(notified_at: DateTime.now)
    @trophy
  end
end