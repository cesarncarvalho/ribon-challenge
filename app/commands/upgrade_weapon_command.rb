class UpgradeWeaponCommand
  def initialize(character_sheet_id, weapon_upgrade_id)
    @character_sheet = CharacterSheet.find(character_sheet_id)
    @new_weapon_upgrade = WeaponUpgrade.find(weapon_upgrade_id)
  end

  def execute
    decrease_character_money
    upgrade_character_weapon
    save_changes
    @character_sheet
  end

  private

  def decrease_character_money
    @character_sheet.coins -= @new_weapon_upgrade.price
  end

  def save_changes
    @character_sheet.save!
  end

  def upgrade_character_weapon
    @character_sheet.weapon_upgrade = @new_weapon_upgrade
  end
end