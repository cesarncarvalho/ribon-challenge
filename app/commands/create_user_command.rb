class CreateUserCommand
  def initialize(username)
    @username = username
  end

  def execute
    User.create!(username: @username, joined_at: DateTime.now)
  end
end