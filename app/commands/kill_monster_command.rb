class KillMonsterCommand
  def initialize(character_sheet_id, monster_id)
    @character_sheet = CharacterSheet.find(character_sheet_id)
    @monster = Monster.find(monster_id)
  end

  def execute
    ActiveRecord::Base.transaction do
      create_collected_coin_record
      create_killed_monster_record
      increase_defeated_monsters_count
      collect_monster_reward
      save_changes
      unlock_trophies_for_killing_monsters
      unlock_trophies_for_collecting_coins
      @character_sheet
    end
  end

  private

  def collect_monster_reward
    @character_sheet.coins += @monster.monster_sheet.reward
  end

  def create_collected_coin_record
    CollectedCoin.create!(user_id: @character_sheet.user_id, value: @monster.monster_sheet.reward)
  end

  def create_killed_monster_record
    KilledMonster.create!(user_id: @character_sheet.user_id, monster_id: @monster.id)
  end

  def increase_defeated_monsters_count
    @character_sheet.defeated_monsters += 1
  end

  def save_changes
    @character_sheet.save!
  end

  def unlock_trophies_for_killing_monsters
    defeated_monsters_of_this_type = KilledMonster.where(user_id: @character_sheet.user_id, monster_id: @monster.id).count
    CollectTrophiesCommand.new(@character_sheet.user_id, 'monsters', defeated_monsters_of_this_type, @monster.id).execute
  end

  def unlock_trophies_for_collecting_coins
    total_collected_coins = CollectedCoin.where(user_id: @character_sheet.user_id).sum(:value)
    CollectTrophiesCommand.new(@character_sheet.user_id, 'coins', total_collected_coins).execute
  end
end