class UpgradeCharacterArmorCommand
  def initialize(character_sheet_id, armor_upgrade_id)
    @character_sheet = CharacterSheet.find(character_sheet_id)
    @new_armor_upgrade = ArmorUpgrade.find(armor_upgrade_id)
  end

  def execute
    decrease_character_money
    upgrade_character_armor
    save_changes
    @character_sheet
  end

  private

  def decrease_character_money
    @character_sheet.coins -= @new_armor_upgrade.price
  end

  def save_changes
    @character_sheet.save!
  end

  def upgrade_character_armor
    @character_sheet.armor_upgrade = @new_armor_upgrade
  end
end