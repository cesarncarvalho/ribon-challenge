class CreateCharacterClasses < ActiveRecord::Migration[6.0]
  def change
    create_table :character_classes do |t|
      t.string :name, null: false
      t.string :description, null: false
      t.integer :attack_bonus, null: false
      t.integer :damage, null: false
      t.integer :hit_points, null: false
      t.string :animation_url, null: false
    end
  end
end
