class CreateWeaponUpgrades < ActiveRecord::Migration[6.0]
  def change
    create_table :weapon_upgrades do |t|
      t.integer :level, null: false
      t.integer :price, null: false
      t.integer :attack_bonus, null: false
      t.integer :damage_bonus, null: false
    end
  end
end
