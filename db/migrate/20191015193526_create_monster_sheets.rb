class CreateMonsterSheets < ActiveRecord::Migration[6.0]
  def change
    create_table :monster_sheets do |t|
      t.belongs_to :monster, foreign_key: true, index: true

      t.integer :level, null: false
      t.integer :attack_bonus, null: false
      t.integer :damage, null: false
      t.integer :hit_points, null: false
      t.integer :reward, null: false
      t.string :animation_url, null: false
    end
  end
end
