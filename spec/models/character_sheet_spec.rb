require 'rails_helper'

RSpec.describe CharacterSheet, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:armor_upgrade).class_name('ArmorUpgrade').optional }
    it { is_expected.to belong_to(:user).class_name('User') }
    it { is_expected.to belong_to(:character_class).class_name('CharacterClass') }
    it { is_expected.to belong_to(:weapon_upgrade).class_name('WeaponUpgrade').optional }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:coins) }
    it { is_expected.to validate_presence_of(:deaths) }
    it { is_expected.to validate_presence_of(:defeated_monsters) }
  end
end
