require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:character_sheets) }
    it { is_expected.to have_many(:collected_coins) }
    it { is_expected.to have_many(:deaths) }
    it { is_expected.to have_many(:killed_monsters) }
    it { is_expected.to have_many(:trophies) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:username) }
    it { is_expected.to validate_presence_of(:joined_at) }
  end
end
