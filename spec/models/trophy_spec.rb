require 'rails_helper'

RSpec.describe Trophy, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:user).class_name('User') }
    it { is_expected.to belong_to(:achievement).class_name('Achievement') }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:conquered_at) }
  end
end
