require 'rails_helper'

RSpec.describe MonsterSheet, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:monster).class_name('Monster') }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:animation_url) }
    it { is_expected.to validate_presence_of(:attack_bonus) }
    it { is_expected.to validate_presence_of(:damage) }
    it { is_expected.to validate_presence_of(:hit_points) }
    it { is_expected.to validate_presence_of(:level) }
    it { is_expected.to validate_presence_of(:reward) }
  end
end
