require 'rails_helper'

RSpec.describe Death, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:user).class_name('User') }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:timestamp) }
  end
end
