require 'rails_helper'

RSpec.describe KilledMonster, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:user).class_name('User') }
    it { is_expected.to belong_to(:monster).class_name('Monster') }
  end
end
