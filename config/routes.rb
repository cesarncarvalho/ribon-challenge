Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'game#play'

  namespace 'api' do
    resources :achievements, only: :index
    resources :armor_upgrades, only: :index
    resources :character_classes, only: :index
    resources :character_sheets, only: :create do
      scope module: :character_sheets do
        resource :armor_upgrade, only: :update
        resource :weapon_upgrade, only: :update
        resources :collected_coins, only: :create
        resources :deaths, only: :create
        resources :killed_monsters, only: :create
      end
    end
    resources :monsters, only: %i[index show]
    resources :trophies, only: %i[index update]
    resources :users, only: %i[create index show], param: :username
    resources :weapon_upgrades, only: :index
  end
end
